#include <string.h>
#include <stdio.h>
#include<math.h>

unsigned short int base2int(char base){
  /* convert a base char to an integer 
   * using the following scheme:
   *  A=0; T=1; G=2; C=3 
   */
  switch(base){
    case 'A':
    case 'a':
      return 0;
    case 'T':
    case 't':
      return 1;
    case 'G':
    case 'g':
      return 2;
    case 'C':
    case 'c':
      return 3;
    default:
      return 4; //Write error handler based on false return value
  }
}

int ipow(int base, int exp){
    /* Fast integer exponentiation */
    int result = 1;
    for (;;){
        if (exp & 1)
            result *= base;
        exp >>= 1;
        if (!exp)
            break;
        base *= base;
    }
    return result;
}

unsigned long long int kmer2int(char* kmer){
  /*    
   * Convert a kmer from base4 to base10
   */
  int i,n;
  unsigned long long int value=0;
  
  n = strlen(kmer);
  for(i=0;i<n;i++){
    /* Pre-compute a table of exponents for sanppy conversion */
    value += base2int(kmer[i])*ipow(n, n-i-1);
  }
  return value;
}
