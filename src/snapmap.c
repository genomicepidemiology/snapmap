#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

#include "snapmap.h"

typedef struct Node{
    int taxid;
    struct Node *next;
} Node;


int push_front( Node **head, int taxid )
{
    Node *new_node = malloc( sizeof( Node ) );
    int success = new_node != NULL;

    if ( success )
    {
        new_node->taxid = taxid;
        new_node->next = *head;
        *head = new_node;
    }

    return success;
}

void output( Node **head )
{
    for( Node *current =*head; current != NULL; current = current->next )
    {
        printf( "%d ", current->taxid );
    }
    printf( "%s", "NULL" );
}

void display( Node **set, size_t n )
{
    for ( size_t i = 0; i < n; i++ )
    {
        output( set++ );
        putchar( '\n' );
    }
}

#define kmer_length   4

int main(void){
    int i, j, dummy, nkmers;
    char* genome = "atagatcgaggtctaagtccgagt";
    char* kmer;
    int taxid=5;
    int N = pow(4,kmer_length);
    int *kmers;
    Node * index[N];

    for(i=0;i<N;i++){
      index[i] = NULL;
    }
    
    nkmers = strlen(genome)-kmer_length;
    kmers = malloc(nkmers*sizeof(int));
    kmer = malloc(kmer_length*sizeof(char));
  
    for(i=0;i<nkmers;i++){
	memcpy( kmer, &genome[i], kmer_length );
	kmers[i] = kmer2int(kmer); 
    }
    
    for(i=0;i<nkmers;i++){
        push_front( &index[kmers[i]], taxid );
    }

//    display( index, sizeof( index ) / sizeof( *index ) );

    


    return 0;
}
