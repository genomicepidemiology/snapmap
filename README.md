# README #

Codename: snapmap

An overwhelmingly fast probabilistic mapper with a minimal memory footprint. 

### What is this repository for? ###

* Map reads against every known reference species in the world (and beyond!)
* Provide probability distribution per read for a range of host species
* Do it quickly
* ...on a laptop

### How do I get set up? ###

* you don't

### Contribution guidelines ###

* Please don't contribute

### Who do I talk to? ###

* Christian Brinch (cbri@food.dtu.dk)
