use std::io;

fn main() {
    // let genome = "GTTTCAGCCTTAGTCATTATCGACTTTTGTTCGAGTGGAGTCCGCCGTGTCACTTTCGCTTTGGCAGCAGTGTCTTGCCCGATTGCAGGATGAGTTACCAGCCACAGAATTCAGTATGTGGATACGCCCATTGCAGGCGGAACTGAGCGATAACACGCTGGCCCTGTACGCGCCAAACCGTTTTGTCCTCGATTGGGTACGGGACAAGTACCTTAATAATATCAATGGACTGCTAACCAGTTTCTGCGGAGCGGATGCCCCACAGCTGCGTTTTGAAGTCGGCACCAAACCGGTGACGCAAACGCCACAAGCGGCAGTGACGAGCAACGTCGCGGCCCCTGCACAGGTGGCGCAAACGCAGCCGCAACGTGCTGCGCCTTCTACGCGCTCAGGTTGGGATAACGTCCCGGCCCCGGCAGAACCGACCTATCGTTCTAACGTAAACGTCAAACACACGTTTGATAACTTCGTTGAAGGTAAATCTAACCAACTGGCGCGCGCGGCGGCTCGCCAGGTGGCGGATAACCCTGGCGGTGCCTATAACCCGTTGTTCCTTTATGGCGGCACGGGTCTGGGTAAAACTCACCTGCTGCATGCGGTGGGTAACGGCATTATGGCGCGCAAGCCGAATGCCAAAGTGGTTTATATGCACTCCGAGCGCTTTGTTCAGGACATGGTTAAAGCCCTGCAAAACAACGCGATCGAAGAGTTTAAACGCTACTACCGTTCCGTAGATGCACTGCTGATCGACGATATTCAGTTTTTTGCTAATAAAGAACGATCTCAGGAAGAGTTTTTCCACACCTTCAACGCCCTGCTGGAAGGTAATCAACAGATCATTCTCACCTCGGATCGCTATCCGAAAGAGATCAACGGCGTTGAGGATCGTTTGAAATCCCGCTTCGGTTGGGGACTGACTGTGGCGATCGAACCGCCAGAGCTGGAAACCCGTGTGGCGATCCTGATGAAAAAGGCCGACGAAAACGACATTCGTTTGCCGGGCGAAGTGGCGTTCTTTATCGCCAAGCGTCTACGATCTAACGTACGTGAGCTGGAAGGGGCGCTGAACCGCGTCATTGCCAATGCCAACTTTACCGGACGGGCGATCACCATCGACTTCGTGCGTGAGGCGCTGCGCGACTTGCTGGCATTGCAGGAAAAACTGGTCACCATCGACAATATTCAGAAGACGGTGGCGGAGTACTACAAGATCAAAGTCGCGGATCTCCTTTCCAAGCGTCGATCCCGCTCGGTGGCGCGTCCGCGCCAGATGGCGATGGCGCTGGCGAAAGAGCTGACTAACCACAGTCTGCCGGAGATTGGCGATGCGTTTGGTGGTCGTGACCACACGACGGTGCTTCATGCCTGCCGTAAGATCGAGCAGTTGCGTGAAGAGAGCCACGATATCAAAGAAGATTTTTCAAATTTAATCAGAACATTGTCATCGTAAACCTATGAAATTTACCGTAGAACGTGAGCATTTATTAAAACCGCTACAACAGGTGAGCGGTCCGTTAGGTGGTCGTCCTACGCTACCGATTCTCGGTAATCTGCTGTTACAGGTTGCTGACGGTACGTTGTCGCTGACCGGTACTGATCTCGAGATGGAAATGGTGGCACGTGTTGCGCTGGTTCAGCCACACGAGCCAGGAGCGACGACCGTTCCGGCGCGCAAATTCT";

    let mut genome = String::new();

    io::stdin()
        .read_line(&mut genome)
        .expect("Falied to read genome");

    const K: usize = 32;
    let max_kmer_bit = max_bits(K * 2);
    let kmer_overflow_bits = usize::MAX - max_kmer_bit;

    let mut genome_index = 0;
    let mut kmer: usize = 0;

    for nuc in genome.trim().chars() {
        genome_index += 1;

        kmer <<= 2;
        kmer += nuc2int(nuc);
        while kmer > max_kmer_bit {
            kmer -= max_kmer_bit + 1
        }

        if genome_index >= K {
            println!("{}", kmer);
            println!("{}", rev_comp(kmer, kmer_overflow_bits, K));
        }
    }

}

fn nuc2int(nuc: char) -> usize {
    match nuc {
        'A' => 0,
        'a' => 0,
        'T' => 3,
        't' => 3,
        'G' => 2,
        'g' => 2,
        'C' => 1,
        'c' => 1,
        _ => unreachable!()
    }
}

fn max_bits(b: usize) -> usize {
    if (usize::BITS as usize) == b {
        return usize::MAX
    } else {
        return (1 << b) - 1
    }
}

fn rev_comp(kmer: usize, overflow: usize, k: usize) -> usize {
    let out_kmer = !kmer - overflow;
    rev_kmer(out_kmer, k)
}

fn rev_kmer(kmer: usize, k: usize) -> usize {
    let mut v = kmer;
    let mut r = kmer;

    let mut s = 8 * core::mem::size_of::<usize>() - 2;
    v >>= 2;

    while v != 0 {  // Quit early if there are no more 1s to shift in
        r <<= 2;    // Make room for the next significant pair of bits
        r |= v & 2; // Add the bit to the reverse variable at second pos
        r |= v & 1; // Add the bit to the reverse variable at first pos
        v >>= 2;    // Go to the next significant pair of bits
        s -= 2;     // Decrement the leftover bit count
    }

    // Shift the reversal to the correct position and return the reversal with
    // respect to kmer size.

    r <<= s;

    r >> (usize::BITS as usize) - k * 2

    // let shift_size = k * 2 + s;

    // if shift_size > (usize::BITS as usize) {
    //    return r << s
    //} else {
    //    return r >> (usize::BITS as usize) - shift_size
    //}

}
