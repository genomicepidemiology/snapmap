import pandas as pd

BC = {'a':0, 't':1, 'g':2, 'c':3,
      'A':0, 'T':1, 'G':2, 'C':3}

class index():
    def __init__(self, kmerlength):
        self.map = [[] for _ in range(pow(4,kmerlength))]

    def append(self, kmer, taxid):
        self.map[kmer] += [taxid]

def dec(kmer):
    value = 0
    for i in range(len(kmer)):
        if kmer[i] not in BC.keys():
            break
        value+=BC[kmer[i]]*pow(4, len(kmer)-i-1)
    return value


def main():
    kmerlength = 10

    kmers = index(kmerlength)

    print('Reading taxonomy...')
    taxtable = pd.read_csv('fungi_species.tax', sep=' ', index_col='asc')
    print('Done')
    with open('fungi.fsa') as database:
        sequence = ''
        for line in database:
            if line[0] == '>':
                if sequence != '':
                    for pos in range(len(sequence)-kmerlength):
                        kmer = sequence[pos:pos+kmerlength]
                        kmers.append(dec(kmer), taxid)
                

                sequence = ''
                asc_num = line.split(' ')[0][1:]
                taxid = taxtable.loc[asc_num]['speciesTax']
            else:
                sequence += line
    mean=0
    for i in range(len(kmers.map)):
        mean+= 1/len(kmers.map) * len(kmers.map[i])
    print(mean)
    print(mean*len(kmers.map))

main()
