shell = /bin/sh
CC = gcc -std=gnu99 -m64 -I. -O3
LFLAGS = -lm
OBJ = src/snapmap.o src/baseconvert.o
SRC = src/snapmap.c src/baseconvert.c
TARGET = snapmap


$(TARGET): $(OBJ)
	gcc $(LFLAGS) -o $@ $^

$(OBJS): %.o: %.c
	gcc -c -o $@ $?

clean:
	rm -f *.o *.x a.out
